# README #

This is to automate the installation of ROS and the necessary packages for AER1217 course (Winter 2017/18). This ONLY works on Ubuntu 14.04. See instructions below for Ubuntu 16.04.

Before running, download the bash file, and type in Terminal:
```
sudo chmod +x setupscript_1404.bash
sudo chmod +x usersetup_1404.bash
```

To run the script, use
```
./setupscript_1404.bash
```

After the script finished running, close the Terminal and open another Terminal.

Download and compile the necessary repositories for the lab:
```
./usersetup_1404.bash
```


For users running Ubuntu 16.04, note that ROS Jade is not compatible. You will have to install ROS Kinetic, which uses the scripts below. As the course was previously run using Ubuntu 14.04, there might be issues with using Ubuntu 16.04 with ROS Kinetic - please note that the TAs will do their best to resolve any compatibility issues, but be aware that there might be certain errors that are beyond the TAs' control.

Before running, download the bash file, and type in Terminal:
```
sudo chmod +x setupscript_1604.bash
sudo chmod +x usersetup_1604.bash
```

To run the script, use
```
./setupscript_1604.bash
```

After the script finished running, close the Terminal and open another Terminal.

Download and compile the necessary repositories for the lab:
```
./usersetup_1604.bash
```
