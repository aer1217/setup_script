cd /home/$USER/
mkdir aer1217
cd aer1217
mkdir extras
cd extras
mkdir src
cd src
catkin_init_workspace
git clone https://github.com/ethz-asl/vicon_bridge.git
git clone https://github.com/AutonomyLab/ardrone_autonomy.git
git clone https://bitbucket.org/aer1217/ardrone_tutorials.git
cd ..
catkin_make
. devel/setup.bash 
cd /home/$USER/aer1217/
git clone https://bitbucket.org/aer1217/ardrone_labs.git labs
git clone https://bitbucket.org/aer1217/gazebo_plugins_1604.git gazebo
cd /home/$USER/aer1217/labs/src
catkin_init_workspace
cd ..
catkin_make
. devel/setup.bash 
cd /home/$USER
echo "source /home/$USER/aer1217/extras/devel/setup.bash" >> /home/$USER/.bashrc
echo "source /home/$USER/aer1217/labs/devel/setup.bash" >> /home/$USER/.bashrc
echo "export GAZEBO_PLUGIN_PATH=/home/$USER/aer1217/gazebo/plugins:${GAZEBO_PLUGIN_PATH}" >> /home/$USER/.bashrc
echo "export GAZEBO_MODEL_PATH=/home/$USER/aer1217/gazebo/models:${GAZEBO_MODEL_PATH}" >> /home/$USER/.bashrc
echo "export GAZEBO_RESOURCE_PATH=/home/$USER/aer1217/gazebo/world:${GAZEBO_RESOURCE_PATH}" >> /home/$USER/.bashrc
