#!/bin/bash
read -s -p "Enter password for sudo: " sudoPW
echo $sudoPW | sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
echo -en "\e[1A"
echo -e "\e[0K"
echo $sudoPW | sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
echo -en "\e[1A"
echo -e "\e[0K"
echo $sudoPW | sudo apt-get update
echo -en "\e[1A"
echo -e "\e[0K"
echo $sudoPW | sudo apt-get install -y ros-kinetic-desktop-full
echo -en "\e[1A"
echo -e "\e[0K"
echo $sudoPW | sudo rosdep init
echo -en "\e[1A"
echo -e "\e[0K"
rosdep update
echo "source /opt/ros/kinetic/setup.bash" >> /home/$USER/.bashrc
source /home/$USER/.bashrc
echo $sudoPW | sudo apt-get install -y python-rosinstall python-rosinstall-generator python-wstool build-essential
echo -en "\e[1A"
echo -e "\e[0K"
echo $sudoPW | sudo apt-get install -y daemontools libudev-dev libiw-dev
echo -en "\e[1A"
echo -e "\e[0K"
echo $sudoPW | sudo apt-get install -y libsdl1.2-dev
echo -en "\e[1A"
echo -e "\e[0K"
echo "Please restart terminal after completion"
